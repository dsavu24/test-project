import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Card from 'react-bootstrap/Card';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';

import './QuestionContainer.scss';

const QuestionContainer = ({ questionId, questionText, answersData, selectedAnswersList, setSelectedAnswersList }) => {
    const [answers, setAnswers] = useState([]);
    const [selectedAnswer, setSelectedAnswer] = useState();

    useEffect(() => {
        //make API call (mock data in our case)
        setAnswers(answersData);
    }, [answersData]);

    // handle different styles for the Radio buttons (answers)
    const handleRadioButtonStyle = index => {
        switch (index) {
            case 0:
            case 1:
                return 'outline-danger';
            case 2:
                return 'outline-dark';
            default:
                return 'outline-success';
        }
    };

    // check if there is already an existing answer for a question
    const checkExistingAnswer = questionIdentifier => {
        return selectedAnswersList.some(answer => answer.questionIdentifier === questionIdentifier);
    };

    // handle selected answer for each question, mapping through answers
    const handleSelectedAnswer = (answerValue, questionIdentifier, answer) => {
        setSelectedAnswer(answerValue);

        if (checkExistingAnswer(questionIdentifier)) {
            //update answer's value if it was already added to the list
            let updatedSelectedAnswersList = selectedAnswersList.map(ans => (
                ans.questionIdentifier === questionIdentifier ? { ...ans, answer } : ans
            ));
            setSelectedAnswersList(updatedSelectedAnswersList);
        } else {
            // add answer to the list
            setSelectedAnswersList(prevState => [...prevState, { questionIdentifier, answer }]);
        }
    };

    return (
        <Card>
            <Card.Body>
                <Card.Title>
                    {questionText}
                </Card.Title>
                <ButtonGroup>
                    {answers && answers.length > 0 ? answers.map((answer, index) => (
                        <ToggleButton
                            key={answer.id}
                            id={`answer-${questionId}${answer.id}`}
                            type="radio"
                            name={`answer-${questionId}`}
                            value={answer.value}
                            variant={handleRadioButtonStyle(index)}
                            checked={selectedAnswer === answer.value}
                            onChange={(e) => handleSelectedAnswer(e.currentTarget.value, questionId, answer)}
                        >
                            {answer.label}
                        </ToggleButton>
                    )) :
                        <div>No answers available.</div>
                    }
                </ButtonGroup>
            </Card.Body>
        </Card>
    );
}

QuestionContainer.propTypes = {
    questionId: PropTypes.string.isRequired,
    questionText: PropTypes.string.isRequired,
    answersData: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired,
    })).isRequired,
    selectedAnswersList: PropTypes.any.isRequired,
    setSelectedAnswersList: PropTypes.any.isRequired,
};

export default QuestionContainer;