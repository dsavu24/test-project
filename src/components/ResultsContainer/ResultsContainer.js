import React from 'react';
import PropTypes from 'prop-types';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import testResults from '../../mocks/testResults.json';

const ResultsContainer = ({ score }) => {
  const handleTestResults = scoreValue => {
    if (scoreValue < 12) {
      return testResults[0]
    } 

    return testResults[1];
  };

  return (
    <>
      <Row>
        <Col xs={12} md={12}>
          <h3>Congratulations! You have finished the test!</h3>
        </Col>
      </Row>
      <Row>
        <Col xs={12} md={12}><h4>{handleTestResults(score).title}</h4></Col>
        <Col xs={12} md={12}>{handleTestResults(score).description}</Col>
      </Row>
    </>
  )
}

ResultsContainer.propTypes = {
  score: PropTypes.number.isRequired,
};

export default ResultsContainer;