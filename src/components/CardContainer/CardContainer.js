import React from 'react';
import PropTypes from 'prop-types';
import Card from 'react-bootstrap/Card';

import './CardContainer.scss';

const CardContainer = ({ cardImageUrl, cardDescription, cardPosition, cardTitle }) => (
    <Card>
        <Card.Img variant={cardPosition} src={cardImageUrl} />
        <Card.Body>
            <Card.Title>{cardTitle}</Card.Title>
            <Card.Text>
                {cardDescription}
            </Card.Text>
        </Card.Body>
    </Card>
)

CardContainer.propTypes = {
    cardImageUrl: PropTypes.string.isRequired,
    cardDescription: PropTypes.string.isRequired,
    cardPosition: PropTypes.string.isRequired,
    cardTitle: PropTypes.string.isRequired,
};

export default CardContainer;