import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import cards from './mocks/cards.json';
import CardContainer from './components/CardContainer/CardContainer';

import './App.scss';

const App = () => {
  const [cardDetails, setCardDetails] = useState([]);

  useEffect(() => {
    //make API call (mock data in our case)
    setCardDetails(cards);
  }, [cards]);

  return (
    <Container>
      <h1>Personality Test</h1>
      <h2>Are you an introvert or an extrovert?</h2>

      <Row>
        {cardDetails && cardDetails.length > 0 ? cardDetails.map(card => {
          return (
            <Col xs={12} md={4} key={card.id}>
              <CardContainer
                cardImageUrl={card.imageUrl}
                cardDescription={card.description}
                cardPosition={card.position}
                cardTitle={card.title}
              >
              </CardContainer>
            </Col>
          );
        }) :
          <div>No card details available.</div>
        }
      </Row>

      <Row className="container-center">
        <Col xs={8} md={8} className="text-center">
          <Link to="/test">Take me to the test</Link>
        </Col>
      </Row>
    </Container>
  );
}

export default App;