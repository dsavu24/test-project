import React from 'react';
import { render } from 'react-dom';
import { Route, Routes, BrowserRouter } from 'react-router-dom';
import reportWebVitals from './reportWebVitals';

import App from './App';
import Test from './routes/testPage';
import NotFound from './routes/notFoundPage';

import './index.scss';

const rootElement = document.getElementById('root');

render(
  <BrowserRouter>
    <Routes>
      <Route path='*' element={<NotFound />} />
      <Route exact path="/" element={<App />} />
      <Route exact path="test" element={<Test />} />
    </Routes>
  </BrowserRouter>,
  rootElement
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
