import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

import QuestionContainer from '../components/QuestionContainer/QuestionContainer';
import ResultsContainer from '../components/ResultsContainer/ResultsContainer';
import questions from '../mocks/questions.json';

const Test = () => {
	const [questionsDetails, setQuestionsDetails] = useState([]);
	const [selectedAnswersList, setSelectedAnswersList] = useState([]);
	const [score, setScore] = useState(0);

	useEffect(() => {
		//make API call (mock data in our case)
		setQuestionsDetails(questions);
	}, [questions]);

	// handle final score by adding every answer's value
	const handleSetScore = () => {
		let finalScore = 0;
		selectedAnswersList.map(answerObject => {
			finalScore += Number(answerObject.answer.value);
		});
		setScore(finalScore);
	};

	return (
		<Container>
			<Row>
				<Col xs={12} md={12}>
					<Link to="/">Back to Homepage</Link>
				</Col>
			</Row>

			{score === 0 ? (
				<Row className="container-center">
					<Col xs={12} md={12}>
						{questionsDetails && questionsDetails.length > 0 ? questionsDetails.map(question => (
							<div key={question.id}>
								<QuestionContainer
									questionId={question.id}
									questionText={question.questionText}
									answersData={question.answers}
									setScore={setScore}
									selectedAnswersList={selectedAnswersList}
									setSelectedAnswersList={setSelectedAnswersList}
								/>
							</div>
						)) : <div>No questions available.</div>
						}
					</Col>

					<Col xs={12} md={12} className="text-center">
						<Button className="custom-btn" onClick={handleSetScore} disabled={selectedAnswersList.length < questionsDetails.length}>Finish test</Button>
					</Col>
				</Row>
			) : (
					<ResultsContainer score={score} />
				)}
		</Container>
	);
}

export default Test;